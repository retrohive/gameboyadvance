# gameboycoloradvance

based on https://archive.org/details/no-intro_romsets (early 2021)

ROM not added :
```
❯ tree -hs --dirsfirst
.
├── [981K]  4x4 Off-Roaders (Europe) (Proto).zip
├── [4.6M]  Advance Wars 2 - Black Hole Rising (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [4.0M]  Advance Wars 2 - Black Hole Rising (USA) (Virtual Console).zip
├── [3.2M]  Advance Wars (Europe) (En,Fr,De,Es) (Virtual Console).zip
├── [2.3M]  Advance Wars (USA) (Virtual Console).zip
├── [3.8M]  Akumajou Dracula - Circle of the Moon (Japan) (Virtual Console).zip
├── [1.2M]  Alex Ferguson's Player Manager 2002 ~ Total Soccer Manager (Europe) (En,Fr,De,Es,It,Nl).zip
├── [3.9M]  Battle Network Rockman EXE 2 (Japan) (Rev 1) (Virtual Console).zip
├── [4.2M]  Battle Network Rockman EXE 3 - Black (Japan) (Rev 1) (Virtual Console).zip
├── [4.2M]  Battle Network Rockman EXE 3 (Japan) (Rev 1) (Virtual Console).zip
├── [3.3M]  Battle Network Rockman EXE (Japan) (Virtual Console).zip
├── [ 14K]  [BIOS] Game Boy Advance (Japan) (Debug Version).zip
├── [ 13K]  [BIOS] Game Boy Advance (World) (GameCube).zip
├── [ 13K]  [BIOS] Game Boy Advance (World).zip
├── [1.1M]  Board Game Classics - Backgammon & Chess & Draughts (Europe) (En,Fr,De,Es,It).zip
├── [4.1M]  Castlevania - Akatsuki no Minuet (Japan) (Virtual Console).zip
├── [4.2M]  Castlevania - Aria of Sorrow (Europe) (En,Fr,De) (Virtual Console).zip
├── [4.2M]  Castlevania - Aria of Sorrow (USA) (Virtual Console).zip
├── [3.8M]  Castlevania - Circle of the Moon (USA) (Virtual Console).zip
├── [3.8M]  Castlevania (Europe) (Virtual Console).zip
├── [4.6M]  Chinmoku no Iseki - Estpolis Gaiden (Japan) (Virtual Console).zip
├── [2.2M]  Chou Makaimura R (Japan) (Virtual Console).zip
├── [2.3M]  ChuChu Rocket! (Japan) (En,Ja,Fr,De,Es) (Virtual Console).zip
├── [319K]  Commandos 2 (Japan) (Proto).zip
├── [1.8M]  Contra Advance - The Alien Wars EX (Europe) (Virtual Console).zip
├── [1.8M]  Contra Advance - The Alien Wars EX (USA) (Virtual Console).zip
├── [1.8M]  Contra Hard Spirits (Japan) (En) (Virtual Console).zip
├── [2.0M]  Crazy Chase (Europe) (En,Fr,De).zip
├── [6.0M]  Densetsu no Stafy 2 (Japan) (Virtual Console).zip
├── [6.8M]  Densetsu no Stafy 3 (Japan) (Rev 1) (Virtual Console).zip
├── [3.1M]  Drill Dozer (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [2.5M]  Essence of War, The - Glory Days (Europe) (En,Fr,De,Es,It).zip
├── [ 10M]  Final Fantasy I, II Advance (Japan) (Rev 1) (Virtual Console).zip
├── [4.6M]  Final Fantasy IV Advance (Japan) (Rev 1) (Virtual Console).zip
├── [7.0M]  Final Fantasy Tactics Advance (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [5.2M]  Final Fantasy Tactics Advance (Japan) (Virtual Console).zip
├── [5.5M]  Final Fantasy Tactics Advance (USA) (Virtual Console).zip
├── [4.9M]  Final Fantasy V Advance (Japan) (Virtual Console).zip
├── [5.0M]  Final Fantasy VI Advance (Japan) (Virtual Console).zip
├── [2.4M]  Final Fight One (Europe) (Virtual Console).zip
├── [2.4M]  Final Fight One (Japan) (Virtual Console).zip
├── [2.3M]  Final Fight One (USA) (Virtual Console).zip
├── [ 12M]  Fire Emblem (Europe) (En,Es,It) (Virtual Console).zip
├── [ 12M]  Fire Emblem (Europe) (En,Fr,De) (Virtual Console).zip
├── [5.5M]  Fire Emblem - Fuuin no Tsurugi (Japan) (Virtual Console).zip
├── [ 11M]  Fire Emblem - Rekka no Ken (Japan) (Virtual Console).zip
├── [9.7M]  Fire Emblem - Seima no Kousek (Japan) (Virtual Console).zip
├── [ 13M]  Fire Emblem - The Sacred Stones (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [9.4M]  Fire Emblem - The Sacred Stones (USA) (Virtual Console).zip
├── [ 11M]  Fire Emblem (USA) (Virtual Console).zip
├── [6.2M]  F-Zero - Climax (Japan) (Virtual Console).zip
├── [6.0M]  F-Zero - Falcon Densetsu (Japan) (Virtual Console).zip
├── [2.1M]  F-Zero for Game Boy Advance (Japan) (Virtual Console).zip
├── [6.6M]  F-Zero - GP Legend (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [5.9M]  F-Zero - GP Legend (USA) (Virtual Console).zip
├── [2.1M]  F-Zero - Maximum Velocity (USA, Europe) (Virtual Console).zip
├── [2.6M]  Game Boy Gallery 4 (Japan) (Virtual Console).zip
├── [8.9M]  Game Boy Wars Advance 1+2 (Japan) (Virtual Console).zip
├── [2.6M]  Game & Watch Gallery 4 (USA) (Virtual Console).zip
├── [2.6M]  Game & Watch Gallery Advance (Europe) (Virtual Console).zip
├── [ 12M]  Golden Sun - The Lost Age (USA, Europe) (Virtual Console).zip
├── [4.2M]  Guranbo (Japan) (Virtual Console).zip
├── [5.0M]  Gyakuten Saiban 2 (Japan) (Virtual Console).zip
├── [5.5M]  Gyakuten Saiban 3 (Japan) (Virtual Console).zip
├── [4.0M]  Gyakuten Saiban (Japan) (Rev 1) (Virtual Console).zip
├── [4.6M]  Hobbit, The - The Prelude to the Lord of the Rings (Europe) (En,Fr,De,Es,It).zip
├── [4.6M]  Hobbit, The - The Prelude to the Lord of the Rings (USA).zip
├── [5.5M]  Hoshi no Kirby - Kagami no Daimeikyuu (Japan) (Virtual Console).zip
├── [4.5M]  Hoshi no Kirby - Yume no Izumi Deluxe (Japan) (Virtual Console).zip
├── [1.2M]  Kawa no Nushi Tsuri 3 & 4 (Japan) (Virtual Console).zip
├── [2.8M]  Kaze no Klonoa G2 - Dream Champ Tournament (Japan) (Virtual Console).zip
├── [7.2M]  Kirby - Nightmare in Dream Land (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [4.5M]  Kirby - Nightmare in Dream Land (USA) (Virtual Console).zip
├── [6.8M]  Kirby & The Amazing Mirror (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [5.8M]  Kirby & The Amazing Mirror (USA) (Virtual Console).zip
├── [2.8M]  Klonoa 2 - Dream Champ Tournament (USA) (Virtual Console).zip
├── [1.7M]  Konami Krazy Racers (Europe) (Virtual Console).zip
├── [1.7M]  Konami Krazy Racers (USA) (Virtual Console).zip
├── [1.7M]  Konami Wai Wai Racing Advance (Japan) (Virtual Console).zip
├── [3.8M]  Kotoba no Puzzle - Mojipittan Advance (Japan) (Virtual Console).zip
├── [4.7M]  Made in Wario (Japan) (Virtual Console).zip
├── [4.5M]  Magical Vacation (Japan) (Virtual Console).zip
├── [ 12M]  Mario Golf - Advance Tour (Europe) (Virtual Console).zip
├── [ 11M]  Mario Golf - Advance Tour (USA) (Virtual Console).zip
├── [ 11M]  Mario Golf - GBA Tour (Japan) (Virtual Console).zip
├── [2.2M]  Mario Kart Advance (Japan) (3DS Virtual Console).zip
├── [2.2M]  Mario Kart Advance (Japan) (Virtual Console).zip
├── [2.1M]  Mario Kart - Super Circuit (Europe) (Virtual Console).zip
├── [2.1M]  Mario Kart - Super Circuit (USA) (Virtual Console).zip
├── [6.3M]  Mario & Luigi RPG (Japan) (Virtual Console).zip
├── [6.6M]  Mario & Luigi - Superstar Saga (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [6.6M]  Mario & Luigi - Superstar Saga (USA) (Virtual Console).zip
├── [6.5M]  Mario Party Advance (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [5.9M]  Mario Party Advance (Japan) (Virtual Console).zip
├── [6.0M]  Mario Party Advance (USA) (Virtual Console).zip
├── [4.9M]  Mario Pinball Land (USA) (Virtual Console).zip
├── [ 11M]  Mario Power Tennis (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [9.7M]  Mario Tennis Advance (Japan) (Virtual Console).zip
├── [ 11M]  Mario Tennis - Power Tour (USA) (En,Fr,De,Es,It) (Virtual Console).zip
├── [3.6M]  Medabots AX - Metabee Ver. (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [3.4M]  Medabots AX - Metabee Ver. (USA) (Virtual Console).zip
├── [3.6M]  Medabots AX - Rokusho Ver. (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [3.4M]  Medabots AX - Rokusho Ver. (USA) (Virtual Console).zip
├── [5.5M]  Medabots - Metabee (Europe) (Virtual Console).zip
├── [5.5M]  Medabots - Metabee (USA) (Virtual Console).zip
├── [5.5M]  Medabots - Rokusho (Europe) (Virtual Console).zip
├── [5.5M]  Medabots - Rokusho (USA) (Virtual Console).zip
├── [3.8M]  Medarot G - Kabuto (Japan) (Virtual Console).zip
├── [3.8M]  Medarot G - Kuwagata (Japan) (Virtual Console).zip
├── [5.4M]  Medarot Navi - Kabuto (Japan) (Virtual Console).zip
├── [5.4M]  Medarot Navi - Kuwagata (Japan) (Virtual Console).zip
├── [5.5M]  Medarot Ni Core - Kabuto (Japan) (Rev 1) (Virtual Console).zip
├── [5.5M]  Medarot Ni Core - Kuwagata (Japan) (Virtual Console).zip
├── [4.6M]  Mega Man & Bass (Europe) (Virtual Console).zip
├── [4.6M]  Mega Man & Bass (USA) (Virtual Console).zip
├── [2.4M]  Mega Man Battle Chip Challenge (Europe) (Virtual Console).zip
├── [2.4M]  Mega Man Battle Chip Challenge (USA) (Virtual Console).zip
├── [3.8M]  Mega Man Battle Network 2 (USA, Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 3 - Blue (Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 3 - Blue Version (USA) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 3 - White (Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 3 - White Version (USA) (Virtual Console).zip
├── [4.2M]  Mega Man Battle Network 4 - Blue Moon (Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 4 - Blue Moon (USA) (Virtual Console).zip
├── [4.2M]  Mega Man Battle Network 4 - Red Sun (Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 4 - Red Sun (USA) (Virtual Console).zip
├── [3.9M]  Mega Man Battle Network 5 - Team Colonel (Europe) (Virtual Console).zip
├── [3.9M]  Mega Man Battle Network 5 - Team Colonel (USA) (Virtual Console).zip
├── [3.9M]  Mega Man Battle Network 5 - Team Proto Man (Europe) (Virtual Console).zip
├── [3.9M]  Mega Man Battle Network 5 - Team Proto Man (USA) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 6 - Cybeast Falzar (Europe) (Virtual Console).zip
├── [4.1M]  Mega Man Battle Network 6 - Cybeast Falzar (USA) (Virtual Console).zip
├── [4.0M]  Mega Man Battle Network 6 - Cybeast Gregar (Europe) (Virtual Console).zip
├── [4.0M]  Mega Man Battle Network 6 - Cybeast Gregar (USA) (Virtual Console).zip
├── [3.3M]  Mega Man Battle Network (Europe) (Virtual Console).zip
├── [3.2M]  Mega Man Battle Network (USA) (Virtual Console).zip
├── [4.2M]  Mega Man Zero 2 (Europe) (Virtual Console).zip
├── [4.2M]  Mega Man Zero 2 (USA) (Virtual Console).zip
├── [4.6M]  Mega Man Zero 3 (Europe) (Virtual Console).zip
├── [4.6M]  Mega Man Zero 3 (USA) (Virtual Console).zip
├── [9.8M]  Mega Man Zero 4 (Europe) (Virtual Console).zip
├── [9.8M]  Mega Man Zero 4 (USA) (Virtual Console).zip
├── [3.5M]  Mega Man Zero (USA) (Virtual Console).zip
├── [4.2M]  Metal Slug - Super Vehicle-001 (Japan) (En) (Proto).zip
├── [4.5M]  Metroid Fusion (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [4.8M]  Metroid Fusion (Japan) (Virtual Console).zip
├── [4.5M]  Metroid Fusion (USA) (Virtual Console).zip
├── [5.0M]  Metroid - Zero Mission (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [5.0M]  Metroid - Zero Mission (Japan) (Virtual Console).zip
├── [5.0M]  Metroid - Zero Mission (USA) (Virtual Console).zip
├── [ 15M]  Mother 3 (Japan) (Virtual Console).zip
├── [2.2M]  Mr. Driller 2 (Europe) (Virtual Console).zip
├── [2.3M]  Mr. Driller 2 (Japan) (Virtual Console).zip
├── [2.2M]  Mr. Driller 2 (USA) (Virtual Console).zip
├── [3.6M]  Mr. Driller A - Fushigi na Pacteria (Japan) (Virtual Console).zip
├── [3.8M]  Napoleon (Japan) (Virtual Console).zip
├── [ 12M]  Ougon no Taiyou - Ushinawareshi Toki (Japan) (Virtual Console).zip
├── [2.6M]  Pac-Man Collection (Europe) (Virtual Console).zip
├── [2.6M]  Pac-Man Collection (Japan) (En) (Virtual Console).zip
├── [2.6M]  Pac-Man Collection (USA) (Virtual Console).zip
├── [1.5M]  Pocket Professor - Kwik Notes - Volume One (USA).zip
├── [1.2M]  Polarium Advance (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [1.1M]  Polarium Advance (USA) (Virtual Console).zip
├── [1.6M]  Revenge of the Smurfs, The (Europe) (En,Fr,De,Es,It,Nl).zip
├── [1.9M]  Ripping Friends, The - The World's Most Manly Men! (USA, Europe).zip
├── [3.4M]  Rockman EXE 4.5 - Real Operation (Japan) (Virtual Console).zip
├── [4.1M]  Rockman EXE 4 - Tournament Blue Moon (Japan) (Rev 1) (Virtual Console).zip
├── [4.1M]  Rockman EXE 4 - Tournament Red Sun (Japan) (Rev 1) (Virtual Console).zip
├── [4.0M]  Rockman EXE 5 - Team of Blues (Japan) (Virtual Console).zip
├── [4.0M]  Rockman EXE 5 - Team of Colonel (Japan) (Virtual Console).zip
├── [4.1M]  Rockman EXE 6 - Dennoujuu Falzar (Japan) (Virtual Console).zip
├── [4.0M]  Rockman EXE 6 - Dennoujuu Gregar (Japan) (Virtual Console).zip
├── [2.5M]  Rockman EXE Battle Chip GP (Japan) (Virtual Console).zip
├── [4.6M]  Rockman & Forte (Japan) (Virtual Console).zip
├── [4.2M]  Rockman Zero 2 (Japan) (Virtual Console).zip
├── [4.6M]  Rockman Zero 3 (Japan) (Virtual Console).zip
├── [9.8M]  Rockman Zero 4 (Japan) (Virtual Console).zip
├── [3.5M]  Rockman Zero (Japan) (Virtual Console).zip
├── [5.1M]  Sennen Kazoku (Japan) (Rev 1) (Virtual Console).zip
├── [4.5M]  Shining Force - Kuroki Ryuu no Fukkatsu (Japan) (Virtual Console).zip
├── [6.1M]  Shining Soul II (Japan) (Virtual Console).zip
├── [2.4M]  Shining Soul (Japan) (Virtual Console).zip
├── [4.3M]  Sonic Advance 2 (Japan) (En,Ja,Fr,De,Es,It) (Virtual Console).zip
├── [4.2M]  Sonic Advance 3 (Japan) (En,Ja,Fr,De,Es,It) (Virtual Console).zip
├── [2.6M]  Sonic Advance (Japan) (En,Ja) (Rev 1) (Virtual Console).zip
├── [3.3M]  Sportsmans Pack 2 in 1 - Cabela's Big Game Hunter + Rapala Pro Fishing (USA).zip
├── [8.3M]  Spyro Superpack - Spyro - Season of Ice + Spyro 2 - Season of Flame (USA).zip
├── [2.2M]  Super Ghouls'n Ghosts (USA) (Virtual Console).zip
├── [2.6M]  Super Mario Advance 3 - Yoshi's Island (Europe) (En,Fr,De,Es,It) (Virtual Console).zip
├── [2.5M]  Super Mario Advance 3 - Yoshi's Island + Mario Brothers (Japan) (Virtual Console).zip
├── [2.5M]  Super Mario Advance 3 - Yoshi's Island (USA) (Virtual Console).zip
├── [2.4M]  Super Mario Advance 4 - Super Mario 3 + Mario Brothers (Japan) (Rev 2) (Virtual Console).zip
├── [2.6M]  Super Mario Advance 4 - Super Mario Bros. 3 (Europe) (En,Fr,De,Es,It) (Rev 1) (Virtual Console).zip
├── [2.4M]  Super Mario Advance 4 - Super Mario Bros. 3 (USA) (Rev 1) (Virtual Console).zip
├── [2.1M]  Super Mario Advance - Super Mario USA + Mario Brothers (Japan) (Virtual Console).zip
├── [2.1M]  Super Mario Advance (USA, Europe) (Virtual Console).zip
├── [4.9M]  Super Mario Ball (Europe) (Virtual Console).zip
├── [4.9M]  Super Mario Ball (Japan) (Virtual Console).zip
├── [4.8M]  Super Street Fighter II Turbo - Revival (Europe) (Rev 1) (Virtual Console).zip
├── [4.8M]  Super Street Fighter II Turbo - Revival (USA) (Virtual Console).zip
├── [4.8M]  Super Street Fighter II X - Revival (Japan) (Virtual Console).zip
├── [3.2M]  Tomato Adventure (Japan) (Virtual Console).zip
├── [5.3M]  Tottoko Hamutarou - Hamu-Hamu Sports (Japan) (Demo) (Kiosk, GameCube).zip
├── [1.1M]  Tsuukin Hitofude (Japan) (Virtual Console).zip
├── [2.2M]  Vattroller X - Taikenban (Japan) (Demo) (Kiosk, GameCube).zip
├── [719K]  Viewpoint (Unknown) (Techo Demo).zip
├── [3.7M]  Wario Land 4 (USA, Europe) (Virtual Console).zip
├── [3.7M]  Wario Land Advance - Youki no Otakara (Japan) (Virtual Console).zip
├── [4.7M]  WarioWare, Inc. - Mega Microgame$! (USA) (Virtual Console).zip
└── [4.7M]  WarioWare, Inc. - Minigame Mania (Europe) (En,Fr,De,Es,It) (Virtual Console).zip

0 directories, 210 files
```
